# 트랜스포머(Transformer) 모델과 셀프 어텐션(Self-Attention) <sup>[1](#footnote_1)</sup>

> <font size="3">트랜스포머 모델과 셀프 어텐션 메커니즘이 언어 모델링에 어떻게 사용되는지 알아본다.</font>

## 목차

1. [들어가며](./transformer-model-and-self-attention.md#intro)
1. [트랜스포머 구조](./transformer-model-and-self-attention.md#sec_02)
1. [셀프 어텐션 메커니즘](./transformer-model-and-self-attention.md#sec_03)
1. [멀티 헤드(Multi-Head) 어텐션](./transformer-model-and-self-attention.md#sec_04)
1. [위치 인코딩(Positional Encoding)](./transformer-model-and-self-attention.md#sec_05)
1. [인코더(Encoder)와 디코더(Decorder) 스택(Stack)](./transformer-model-and-self-attention.md#sec_06)
1. [트랜스포머 모델의 응용](./transformer-model-and-self-attention.md#sec_07)
1. [프랜스퍼머 모델의 과제와 한계](./transformer-model-and-self-attention.md#sec_08)
1. [맺으며](./transformer-model-and-self-attention.md#summary)

<a name="footnote_1">1</a>: [LLM Tutorial 4 — The Transformer Model and Self-Attention](https://ai.plainenglish.io/llm-tutorial-4-the-transformer-model-and-self-attention-a6b111fe9fa4?sk=07665977d266221e8493f9a69f4afcf1)를 편역하였다.
